FROM node:14.15.0-alpine3.12

ARG USER_ID
ARG GROUP_ID
ARG PROJECT_NAME

RUN deluser --remove-home node

RUN addgroup --gid $GROUP_ID user
RUN adduser -D -g '' -u $USER_ID -G user user

RUN apk update
RUN apk upgrade
RUN apk add bash

RUN mkdir /app
RUN chown user /app
USER user
WORKDIR /app
COPY --chown=user . /app/

RUN chmod +x scaffold.sh
RUN chmod +x start.sh
