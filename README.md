# Madera Starter Front

Ce projet sert de point de départ pour les applications front du projet Madera. Il est basé sur la bibliothèque graphique [React](https://github.com/facebook/react).

## Prérequis
Le seul logiciel nécessaire à l'utilisation du projet est [Docker](https://www.docker.com/).
Le projet requiert également qu'une instance de [madera-infra](https://gitlab.com/olhybrius-madera/madera-infra) soit en cours d'exécution.

## Dépendances
- [react-router-dom@5.2.0](https://github.com/ReactTraining/react-router) : routing de l'application
- [primereact@5.0.1](https://github.com/primefaces/primereact) : bibliothèque de composants
- [primeicons@4.0.0](https://github.com/primefaces/primeicons): icônes pour les composants PrimeReact
- [primeflex@2.0.0](https://github.com/primefaces/primeflex) : gestion de la disposition des composants PrimeReact
- [classnames@2.2.6](https://github.com/JedWatson/classnames): dépendance de PrimeReact
- [react-transition-group@4.4.1](https://github.com/reactjs/react-transition-group): dépendance de PrimeReact
- [keycloak-js@11.0.3](https://github.com/keycloak/keycloak) : intégration de Keycloak
- [@react-keycloak/web@3.4.0](https://github.com/react-keycloak/react-keycloak) : intégration de Keycloak
- [axios@0.21.0](https://github.com/axios/axios): client HTTP
- [dayjs@1.9.8](https://github.com/iamkun/dayjs): manipulation des dates et heures

## Variables d'environnement
Le projet s'appuie essentiellement sur des variables d'environnement à définir dans un fichier .env à la racine du projet. Les valeurs à renseigner sont les suivantes :

- **USER_ID** : L'ID de l'utilisateur au sein du container. Sous Linux, doit être égal à l'ID de votre utilisateur courant. Sous Windows, peu importe.
- **GROUP_ID**: L'ID du groupe de l'utilisateur au sein du container. Sous Linux, doit être égal à l'ID du groupe de votre utilisateur courant. Sous Windows, peu importe.
- **PROJECT_NAME** : Le nom du projet qui sera utilisé à la génération
- **PROJECT_PORT** : Le port d'écoute du projet
- **KEYCLOAK_REALM** : Le royaume Keycloak
- **KEYCLOAK_CLIENT** : Le nom du client Keycloak à utiliser
- **KEYCLOAK_AUTH_SERVER_URL** : L'URL complète du serveur Keycloak
- **BACKEND_HOST** : Le nom d'hôte ou adresse IP du serveur hébergeant l'application back-end
- **BACKEND_PORT** : Le port d'écoute de l'application back-end


## Générer le projet de base
Lancer la commande suivante à la racine du projet :

```
docker-compose run web bash scaffold.sh
```

## Démarrer le projet
Lancer la commande suivante à la racine du projet :

```
docker-compose up -d
```

## Arrêter le projet
Lancer la commande suivante à la racine du projet :

```
docker-compose down -d
```

## Intégration dans WebStorm

### Lancer le projet depuis l'interface graphique
1. Cliquer sur "Edit Configuration".
2. Dans le menu de gauche, cliquer sur "+".
3. Donner un nom (champ "Name") à la configuration.
4. Dans le champ "Configuration file(s)", sélectionner le fichier docker-compose.yml du projet (./docker-compose.yml).
5. Cliquer sur "OK".
